﻿using Microsoft.Extensions.Configuration;
using mqttclient;
using MQTTnet;
using MQTTnet.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static ManualResetEvent _quitEvent = new ManualResetEvent(false);

        static void Main(string[] args)
        {
            Console.WriteLine("Iniciando cliente MQTT!");
            Console.CancelKeyPress += (sender, eArgs) => {
                _quitEvent.Set();
                eArgs.Cancel = true;
            };

            //Recupera as configuracoes do arquivo
            var builder =
                    new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);


            // kick off asynchronous stuff 
            new MqttSubscriber(builder.Build(), new SerialComunication());
            _quitEvent.WaitOne();

            // cleanup/shutdown and quit
            
        }



    }
}
