﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;

namespace mqttclient
{
    public class SerialComunication
    {
        private readonly SerialDevice serial = null;
        public SerialComunication()
        {

            Console.WriteLine("Pendo portas serial");
            var ports = SerialDevice.GetPortNames();
            bool isTTY = false;
            foreach (var prt in ports)
            {
                Console.WriteLine($"Nome da Serial: {prt}");
                if (prt.Contains("ttyS0"))
                {
                    isTTY = true;
                }
            }
            if (!isTTY)
            {
                Console.WriteLine("Nao foi encontrada a porta serial ttyS0!");
            }
            serial = new SerialDevice("/dev/serial0", BaudRate.B115200);
            serial.DataReceived += ReceberMensagem;
            serial.Open();
            
        }

        public void EnviarMensagem(string mensagem)
        {
            serial.Write(System.Text.Encoding.UTF8.GetBytes(mensagem));
        }


        private static void ReceberMensagem(object arg1, byte[] arg2)
        {
            Console.WriteLine
            (
                $"Mensagem recebida pela serial: {System.Text.Encoding.UTF8.GetString(arg2)}"
            );
        }


    }
}
