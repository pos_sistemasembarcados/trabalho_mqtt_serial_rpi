﻿using Microsoft.Extensions.Configuration;
using MQTTnet;
using MQTTnet.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace mqttclient
{
    public class MqttSubscriber
    {
        private readonly IConfigurationRoot _config;
        private readonly IMqttClient _mqttClient;
        private readonly IMqttClientOptions _options;
        private SerialComunication _serial = null;
        public MqttSubscriber(IConfigurationRoot config, SerialComunication serial)
        {
            _config = config;
            _serial = serial;
            // Create a new MQTT client.
            MqttFactory factory = new MqttFactory();
            _mqttClient = factory.CreateMqttClient();
            // Create TCP based options using the builder.
            _options =
                new MqttClientOptionsBuilder()
                    .WithClientId("raspbetty")
                    .WithTcpServer
                    (
                        _config["ServidorMqtt"],
                        int.Parse(_config["PortaMqtt"] ?? "1883")
                    )
                    //.WithCredentials("bud", "%spencer%")
                    //.WithTls
                    //(
                    //    new MqttClientOptionsBuilderTlsParameters()
                    //    {
                    //        Certificates = new List<IEnumerable<byte>>()
                    //        {
                    //            X509Certificate.CreateFromCertFile(@"ca.crt")
                    //            .Export(X509ContentType.Cert)
                    //        }
                    //    }
                    //)
                    .WithCleanSession()
                    .Build();

            _mqttClient.Connected += MqttConectado;
            _mqttClient.Disconnected += MqttDesonectado;
            _mqttClient.ApplicationMessageReceived += MensagemRecebida;
            _mqttClient.ConnectAsync(_options);
        }

        private async void MqttConectado(object sender, MqttClientConnectedEventArgs e)
        {

            Console.WriteLine("### Conectado ao servidor MQTT ###");
            await _mqttClient.SubscribeAsync(new TopicFilterBuilder().WithTopic("temperatura").Build());
            Console.WriteLine("### Topico temperatura subscrito ###");
        }

        private async void MqttDesonectado(object sender, MqttClientDisconnectedEventArgs e)
        {
            Console.WriteLine("### Disconectado do servidor MQTT ###");
            await Task.Delay(TimeSpan.FromSeconds(5));

            try
            {
                await _mqttClient.ConnectAsync(_options);
            }
            catch
            {
                Console.WriteLine("### Falha ao reconectar ao servidor MQTT ###");
            }
        }

        private void MensagemRecebida(object sender, MqttApplicationMessageReceivedEventArgs e)
        {
            string mensagem = Encoding.UTF8.GetString(e.ApplicationMessage.Payload);
            Console.WriteLine("### Mensagem receida ###");
            Console.WriteLine($"+ Topico = {e.ApplicationMessage.Topic}");
            Console.WriteLine($"+ Mensagem = {mensagem}");
            Console.WriteLine($"+ QoS = {e.ApplicationMessage.QualityOfServiceLevel}");
            Console.WriteLine($"+ Retain = {e.ApplicationMessage.Retain}");
            Console.WriteLine();
            _serial.EnviarMensagem(mensagem);
        }

    }
}
